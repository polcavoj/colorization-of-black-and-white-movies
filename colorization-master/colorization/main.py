import cv2
import os
import subprocess
import shutil
import sys
import colorize


def extract_audio(video_path):
    command = "ffmpeg -i " + video_path + " -ab 160k -ac 2 -ar 44100 -vn ../../data/audio.mp3"

    subprocess.call(command, shell=True)
    return "../../data/audio.mp3"


def video_to_frames(video_path):
    print('Making a frames from video input.')
    frames_folder = '../../data/frames/'

    # if the folder doesn't exist, it is created
    if not os.path.exists(frames_folder):
        os.makedirs(frames_folder)

    video_capture = cv2.VideoCapture(video_path)
    fps = video_capture.get(cv2.CAP_PROP_FPS)
    success, image = video_capture.read()
    count = 0
    while success:
        cv2.imwrite(os.path.join(frames_folder, "%d.jpg" % count), image)
        success, image = video_capture.read()
        count += 1

    print('%d frames was saved.' % count)
    return fps


def numerical_sort(value):
    first, _ = value.split('.')
    return int(first)


def add_audio(audio_in):
    video_path = '../../data/video/output_video.mp4'
    command = "ffmpeg -i " + video_path + " -i " + audio_in + " -c copy -map 0:v -map 1:a " \
              + "../../data/video/final_output_video.mkv"

    subprocess.call(command, shell=True)


def frames_to_video(fps):
    print('Making a video from frames.')
    video_output_path = '../../data/video/output_video.mp4'
    frames_folder = '../../data/colored_frames/'

    images = [img for img in os.listdir(frames_folder) if img.endswith(".jpg")]
    images.sort(key=numerical_sort)

    frame = cv2.imread(os.path.join(frames_folder, images[0]))
    height, width, layers = frame.shape

    # attributes -  1. path to the video
    #               2. codec which is used to compile a video to the mp4 format,
    #               3. fps rate
    #               4. size of the output video
    video = cv2.VideoWriter(video_output_path, cv2.VideoWriter_fourcc(*'mp4v'), fps, (width, height))

    for image in images:
        video.write(cv2.imread(os.path.join(frames_folder, image)))

    cv2.destroyAllWindows()
    video.release()

    print('Video output_video.mp4 was made.')


def clean():
    print('Deleting a frames folder.')
    shutil.rmtree('../../data/frames/')
    os.remove('../../data/audio.mp3')
    # os.remove('../data/video/output_' + video_name)


def colorization():
    print('Colorization process starts.')
    input_frames_folder = '../../data/frames/'
    output_frames_folder = '../../data/colored_frames/'

    # if the folder doesn't exist, it is created
    if not os.path.exists(output_frames_folder):
        os.makedirs(output_frames_folder)

    colorize.main(input_frames_folder, output_frames_folder)


def main():
    if (len(sys.argv) - 1) == 0:
        video_path = '../../data/video/lukas_graham_shorter.mp4'
    else:
        video_path = sys.argv[1]

    if os.path.isfile(video_path):
        audio = extract_audio(video_path)
        fps = video_to_frames(video_path)
        colorization()
        frames_to_video(fps)
        add_audio(audio)

        # clean()
    else:
        print("Input file does not exist.")


if __name__ == '__main__':
    main()
